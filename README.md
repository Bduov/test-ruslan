﻿# Agrostream online task

## Пререквизиты

Убедитесь, что:

- вы можете склонировать репозиторий
- solution билдится без ошибок
- юнит тесты успешно исполняются

Для этого Вам необходимо:

- git
- dotnet SDK
- XUnit test runner (via command line or an IDE)

## Текущее решение

Текущее решение находится в ветке master. Оно содержит библиотеку, которая может парсить CSV файл с данными о клиентах: каждая строка содержит ID, имя, фамилию и статус клиента.

## Задача

The task is to extend the library to implement another parser that can parse files with orders that contain the following data:
Задача расширить библиотеку: написать другой парсер, чтобы он мог парсить файл с заказами:
* Order ID (positive integer)
* Date and time when the order was created (UTC date in [ISO format](https://en.wikipedia.org/wiki/ISO_8601))
* Item ID (positive integer)
* Customer ID (positive integer)
* Amount of items in the order
* Price of each item (in Russian format, i.e. with comma decimal separator)
* Customer comment (string, not required)

Файлы с заказами имеют немного другой формат:
* столбы в каждой строке разделены пробелами, вместо запятой;
* customer comment может содержать пробелы, в этом случае значение заключено в кавычки.

Количество строк в файле не будет превышать 1000. Поэтому, для нас более важным критерием будет: чистый код, хорошая архитектура, test coverage

Пример файла `orders.txt` можно найти в папке `ParsingLibrary.Test`

Выполняя задание руководствуйтесь тем, как вы считаете будет наиболее подходяще (в том числе рефакторинг существующего кода, если необходимо)
Несмотря на то, что сторонние библиотеки для парсинга csv файлов могут быть использованы для этой задачи, нам было бы более интересно увидеть как вы сможете справиться с этой задачей самостоятельно

В данном репозитории нет исполняемого проекта, и не требуется. Доказательство, что решение рабочее - через тесты